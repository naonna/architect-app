package io.androidpro.architect

class OrderListViewModel(private val repo: FakeRepository){
    /**
     * Actions:
     * - open the screen
     * - refresh using button
     * - (optional) - swipe to refresh
     * - open details click
     *
     * Data:
     * - list of orders
     * - error
     * - loading state
     *
     */
}